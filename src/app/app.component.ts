import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'prueba';
  
  public formulario: FormGroup | undefined;
  constructor(private formConstruct: FormBuilder){}

  ngOnInit(): void {
    
  this.formulario = this.formConstruct.group({"periodo":[],"consultor":[] }) ;
  }
}

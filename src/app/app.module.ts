import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoxComponent } from './box/box.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { BoxDetailComponent } from './box-detail/box-detail.component';
import { RightBoxComponent } from './box/right-box/right-box.component';
import { LeftBoxComponent } from './box/left-box/left-box.component';
import { HttpClientModule} from '@angular/common/http';
import { BoxGraficaComponent } from './box-grafica/box-grafica.component'; 
@NgModule({
  declarations: [
    AppComponent,
    BoxComponent,
    HeaderComponent,
    MenuComponent,
    BoxDetailComponent,
    RightBoxComponent,
    LeftBoxComponent,
    BoxGraficaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

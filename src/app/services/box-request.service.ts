import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BoxRequestService {

  constructor(private httpCliente:HttpClient) { }

  public get(url:string){
     return this.httpCliente.get(url);
  }

}

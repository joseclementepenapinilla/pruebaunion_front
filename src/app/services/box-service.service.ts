import { Injectable ,EventEmitter, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BoxServiceService {
@Output() triggerConsultoresSelec:EventEmitter<any> = new EventEmitter(); 
@Output() triggerConsultoresDelSelec:EventEmitter<any> = new EventEmitter(); 
@Output() triggerConsultoresDetails:EventEmitter<any> = new EventEmitter(); 
@Output() triggerConsultoresDataGrafic:EventEmitter<any> = new EventEmitter(); 
@Output() triggerConsultoresDataGraficReset:EventEmitter<any> = new EventEmitter(); 
  constructor() { }
 

}

import { Component, Input, Inject,LOCALE_ID, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BoxServiceService } from '../services/box-service.service';
import { formatCurrency }  from '@angular/common';


@Component({
  selector: 'app-box-detail',
  templateUrl: './box-detail.component.html',
  styleUrls: ['./box-detail.component.css']
})
export class BoxDetailComponent implements OnInit,OnChanges {
@Input() dataRelators:Array<any> = [];
public facturaRelators:Array<any>=[];

constructor(private servicioBox:BoxServiceService, @Inject(LOCALE_ID) public locale: string){}

ngOnInit(): void {
  this.servicioBox.triggerConsultoresDetails.subscribe(data => {
    console.log("llegó data");
    console.log(data);
    this.facturaRelators = data;
  } )
}
ngOnChanges(changes: SimpleChanges): void {
  
}

setRealBrasil(val:any){
  return formatCurrency(val,this.locale,
    'R$')
}
}

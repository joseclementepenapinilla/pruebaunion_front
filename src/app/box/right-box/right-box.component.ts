import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { BoxServiceService } from 'src/app/services/box-service.service';
@Component({
  selector: 'app-right-box',
  templateUrl: './right-box.component.html',
  styleUrls: ['./right-box.component.css']
})
export class RightBoxComponent implements OnInit, OnChanges {
  @Input() consultoresInput: any;
  grupoSelec!: FormGroup;
  public consultoresSelec: Array<any> = [];
  public consultor: string = "";

  constructor(private servicioBox: BoxServiceService) {
    this.grupoSelec = new FormGroup({ listSelected: new FormControl() });

    this.grupoSelec.valueChanges.subscribe(value => {
      console.log(value);
      console.log("consultoresSEL=");
    });
  }

  ngOnInit(): void {
    this.servicioBox.triggerConsultoresSelec.subscribe(data => {
      this.consultoresSelec = data.data;
    })
  }
  ngOnChanges(changes: any): void {
    console.log("camios")
  }

  delConsultor() {
    for (let conSel of this.grupoSelec.value.listSelected) {
      var indice: any;
      indice = this.consultoresSelec.map(function (valor) { return valor.coUsuario; }).indexOf(conSel)
        /**ingresa y elimina de los seleccionados */
        this.consultoresSelec.splice(indice,1);
     
    }    
  }

}

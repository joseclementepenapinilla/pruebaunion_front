import { Component, OnInit } from '@angular/core';
import { BoxServiceService } from '../services/box-service.service';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { BoxRequestService } from '../services/box-request.service';
@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {

  private url:string="http://localhost:8080/";

  public facturaRelators:Array<any> = [];
  public seriesRelatorGrafica:any=[];
  public consultores: Array<any> = [];
  public consultoresSel: Array<any> = [];
  public formulario!: FormGroup;
  
  constructor(private servicioBox: BoxServiceService, private servicioApi: BoxRequestService) {
    this.createForm();
  }

  ngOnInit(): void {

    this.loadConsultores();
  }

  createForm() {
    this.formulario = new FormGroup({
      mesDesde: new FormControl('', [Validators.required]),
      anioDesde: new FormControl('', [Validators.required]),
      mesHasta: new FormControl('', [Validators.required]),
      anioHasta: new FormControl('', [Validators.required]),
      consultores: new FormControl('', [Validators.required]),
      listSelected: new FormControl('', [Validators.required]),
    });

    this.formulario.valueChanges.subscribe(value => {
      console.log(value);
      console.log("consultoresSEL=");
      console.log(this.consultoresSel)
    });
  }

  selConsultor() {
    for (let conSel of this.formulario.value.consultores) {
      var indice: any;
      /**si no existe dentro de los seleccionados retorna -1*/
      if (this.consultoresSel.map(function (valor) { return valor.coUsuario; }).indexOf(conSel) < 0) {
        indice = this.consultores.map(function (valor) { return valor.coUsuario; }).indexOf(conSel)
        /**ingresa e inserta en los seleccionados */
        this.consultoresSel.push(this.consultores[indice]);
      }
    }    
    this.servicioBox.triggerConsultoresSelec.emit({data:this.consultoresSel});
  }
  
  delConsultor() {
    for (let conSel of this.formulario.value.consultores) {
      var indice: any;
      /**si  existe dentro de los seleccionados retorna mayor o igual a 0*/
      if (this.consultoresSel.map(function (valor) { return valor.coUsuario; }).indexOf(conSel) >= 0) {
        indice = this.consultoresSel.map(function (valor) { return valor.coUsuario; }).indexOf(conSel)
        /**ingresa y elimina de los seleccionados */
        this.consultoresSel.splice(indice,1);
      }
    }    
    this.servicioBox.triggerConsultoresSelec.emit({data:this.consultoresSel});
  }

  relatorioAction(formulRel: any){
    this.facturaRelators = [];
    Object.entries(this.consultoresSel).forEach(([key, value], index) => {
      this.servicioApi.get(this.url+"relatorio/ganancias/"+value.coUsuario).subscribe(resp=>{
        console.log("respuesta de servcicio para "+value.coUsuario);
        console.log(resp);
        
        this.facturaRelators.push(<any>resp);  
         
      });      
      
      this.servicioBox.triggerConsultoresDetails.emit(this.facturaRelators);   
      
    });
    
  }

  loadGrafica(){
    this.servicioBox.triggerConsultoresDataGraficReset.emit();
    this.seriesRelatorGrafica=[];
    Object.entries(this.consultoresSel).forEach(([key, value], index) => {
    this.servicioApi.get(this.url+"relatorio/ganancias/grafica/"+value.coUsuario).subscribe(resp=>{
      
    this.servicioBox.triggerConsultoresDataGrafic.emit(resp);  
    });
    });   
  }

  loadConsultores(){
    this.servicioApi.get(this.url+"main/consultor").subscribe(resp=>{
      this.consultores = <any>resp;
    });
    
  }

  private utilPromedioGrafica(input:any){

  }
}

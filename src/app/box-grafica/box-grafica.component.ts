import { Component } from '@angular/core';
import * as Highcharts from 'highcharts';
import { BoxRequestService } from '../services/box-request.service';
import { BoxServiceService } from '../services/box-service.service';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-box-grafica',
  templateUrl: './box-grafica.component.html',
  styleUrls: ['./box-grafica.component.css']
})
export class BoxGraficaComponent {

  
  constructor(private servicioBox: BoxServiceService,private servicioApi: BoxRequestService) {  
  } 
  
public seriesRelatorGrafica:any[]=[];
public i=0;//contador de posiciones (usuarios consultados)
public dataGrafica:any=[];

loadGraficaColumn(){
  var options: any = {
    title: {
        text: 'PErformance',
        align: 'left'
    },
    xAxis: {
        categories: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']
    },
    yAxis: {
        title: {
            text: '$B'
        },
        max:132000
    },
    tooltip: {
        valueSuffix: ' $B'
    },
    series: this.seriesRelatorGrafica
}
  Highcharts.chart('container', options);
}

ngOnInit(){
  this.servicioBox.triggerConsultoresDataGraficReset.subscribe(data=>{
    this.seriesRelatorGrafica=[];
    this.seriesRelatorGrafica.push(this.genSplinePromedio());
    this.i=0;
    this.dataGrafica=[0,0,0,0,0,0,0,0,0,0,0,0];
  })
  this.servicioBox.triggerConsultoresDataGrafic.subscribe(data=>{
  this.seriesRelatorGrafica.push(data);
  this.i++;//contador de posiciones (usuarios consultados)
   
  console.log(this.seriesRelatorGrafica);    
    Object.entries(this.seriesRelatorGrafica).forEach(([k,v],i)=>{
      //omitor primera grafica del conteo
      if(<any>k > 0){
       for( let index=0 ; index < 12 ; index++){
        this.dataGrafica[index] +=v.data[index];
        console.log("sumae"+this.dataGrafica[index]);
        }
      }
    });
    //generar promedio
    for( let index=0; index < this.dataGrafica.length; index++){
      this.dataGrafica[index] = this.dataGrafica[index]/this.i; 
    }
  console.log("suma posicion 1 promediado="+this.dataGrafica);
  console.log("cantidad de usuarios="+this.i);
  this.seriesRelatorGrafica[0].data=this.dataGrafica;
  this.loadGraficaColumn();
  });
  
}

genSplinePromedio(){
    return {
      type: 'spline',
      name: 'Custo Fixo Medio',
      data: [],
      marker: {
          lineWidth: 2,
          lineColor: "#0000",
          fillColor: 'white'
      }
    }
}
}
